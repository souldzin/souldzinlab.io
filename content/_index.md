---
title: "souldzin"
---

Oh hi, I'm Paul!

I'm a software engineer, but I like to do other things too. I don't like to build
personal websites, so this probably isn't going to get too fancy anytime soon...

Currently I work [at GitLab](https://gitlab.com) as a Staff Fullstack Engineer.

Here are some things I made:

{{< project name="conventionalcomments" >}}

{{< yt src="https://www.youtube-nocookie.com/embed/S8uPRHYSDcU" title="The Open Source Function" channel-href="https://www.youtube.com/@theopensourcefunction8032" >}}

{{< yt src="https://www.youtube-nocookie.com/embed/5Bkmz-f1CnE" title="Beyond Functional - On Software Architecture and Requirements" slides-href="./docs/BeyondFunctional.pdf" >}}
