const AT = "@";
const Z = "zin";

const findEmailLinks = () => document.querySelectorAll(".js-email");

const getEmail = () => {
  // Obsfuncate for spammers
  const parts = [
    "com",
    "",
    ".",
    "",
    Z,
    "",
    "uld",
    "",
    "so",
    "",
    AT,
    "act",
    "ont",
    "c",
  ];
  return parts.reverse().join("");
};

const setupEmailLink = (el) => {
  const email = getEmail();
  const href = "mailto:" + email;
  const onClick = () => {
    window.open(href);
  };

  el.removeAttribute("style");
  el.addEventListener("click", onClick);

  // Wait for spam bots to get tired before setting the href
  setTimeout(() => {
    el.removeEventListener("click", onClick);
    el.href = href;
  }, 2000);
};

const main = () => {
  findEmailLinks().forEach(setupEmailLink);
};

document.addEventListener("DOMContentLoaded", main);
